const second = document.getElementById("second");
const dropdown = document.getElementById("dropdown");

second.addEventListener("click", () => {
  if (dropdown.style.display == "none") {
    dropdown.style.display = "flex";
  } else {
    dropdown.style.display = "none";
  }
});
